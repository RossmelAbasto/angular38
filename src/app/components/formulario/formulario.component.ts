import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Ipersona } from 'src/app/interfaces/persona.interface';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  constructor() { }


  persona: Ipersona = {
    nombre: "",
    direccion: "",
  };

  // eslint-disable-next-line @angular-eslint/no-empty-lifecycle-method
  ngOnInit(): void { }

  guardar(formulario: NgForm) {
    console.log("<----Submit Enviado---->");
    console.log(this.persona.nombre);
    console.log(this.persona.direccion);
    formulario.reset();
  }
}